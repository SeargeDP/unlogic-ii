package the_fireplace.unlogicii.compat.jei;

import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import javax.annotation.Nonnull;

/**
 * @author The_Fireplace
 */
public class GunpowderCategory implements IRecipeCategory {
	@Nonnull
	private final IDrawable background;

	public GunpowderCategory(IGuiHelper guiHelper) {
		super();
		ResourceLocation location = new ResourceLocation("unlogicii", "textures/gui/nei_pop_furnace.png");
		background = guiHelper.createDrawable(location, 3, 15, 169, 53);
	}

	@Nonnull
	@Override
	public String getUid() {
		return "unlogicii.pop_furnace.gunpowder";
	}

	@Nonnull
	@Override
	public String getTitle() {
		return StatCollector.translateToLocal("jei.gunpowder");
	}

	@Nonnull
	@Override
	public IDrawable getBackground() {
		return background;
	}

	@Override
	public void drawExtras(Minecraft minecraft) {

	}

	@Override
	public void drawAnimations(Minecraft minecraft) {

	}

	@Override
	public void setRecipe(@Nonnull IRecipeLayout recipeLayout, @Nonnull IRecipeWrapper recipeWrapper) {
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();

		int powderSlot = 10;

		guiItemStacks.init(powderSlot, true, 4, 7);
		guiItemStacks.setFromRecipe(powderSlot, recipeWrapper.getInputs());
	}
}
