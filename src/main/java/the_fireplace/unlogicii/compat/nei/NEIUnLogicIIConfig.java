package the_fireplace.unlogicii.compat.nei;

import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.oredict.OreDictionary;
import the_fireplace.unlogicii.UnLogicII;
/**
 *
 * @author The_Fireplace
 *
 */
@Optional.Interface(iface = "codechicken.nei.api.API", modid = "NotEnoughItems")
public class NEIUnLogicIIConfig implements IConfigureNEI {

	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public void loadConfig() {
		API.hideItem(new ItemStack(UnLogicII.obsidian_farmland, 1, OreDictionary.WILDCARD_VALUE));
		API.hideItem(new ItemStack(UnLogicII.quartz_crop));
		API.hideItem(new ItemStack(UnLogicII.shell));
		API.registerRecipeHandler(new PopFurnaceRecipeHandler());
		API.registerUsageHandler(new PopFurnaceRecipeHandler());
	}

	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public String getName() {
		return UnLogicII.MODNAME;
	}

	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public String getVersion() {
		return UnLogicII.VERSION;
	}
}
