package the_fireplace.unlogicii.compat.nei;

import java.awt.Rectangle;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraftforge.fml.common.Optional;
import the_fireplace.unlogicii.gui.GuiPopFurnace;
import the_fireplace.unlogicii.recipes.PopFurnaceRecipes;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;
/**
 * 
 * @author The_Fireplace
 *
 */
@Optional.Interface(iface = "codechicken.nei.api.API", modid = "NotEnoughItems")
public class PopFurnaceRecipeHandler extends TemplateRecipeHandler {
	@Optional.Interface(iface = "codechicken.nei.api.API", modid = "NotEnoughItems")
	public class PoppingPair extends CachedRecipe{
		public PoppingPair(ItemStack isin, ItemStack isout){
			isin.stackSize=1;
			this.isin=new PositionedStack(isin, 80-5, 23-11);
			this.isout=new PositionedStack(isout, 80-5, 45-11);
		}
		@Optional.Method(modid = "NotEnoughItems")
		@Override
		public List<PositionedStack> getIngredients() {
			return getCycledIngredients(cycleticks / 48, Collections.singletonList(isin));
		}
		@Optional.Method(modid = "NotEnoughItems")
		@Override
		public PositionedStack getResult() {
			return isout;
		}
		PositionedStack isin;
		PositionedStack isout;
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public void loadTransferRects() {
		transferRects.add(new RecipeTransferRect(new Rectangle(80-6, 40-14, 90, 6), "popping"));
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public Class<? extends GuiContainer> getGuiClass() {
		return GuiPopFurnace.class;
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public String getRecipeName() {
		return StatCollector.translateToLocal("tile.pop_furnace.name");
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public void loadCraftingRecipes(String outputId, Object... results) {
		if (outputId.equals("popping") && getClass() == PopFurnaceRecipeHandler.class) {//don't want subclasses getting a hold of this
			Map<ItemStack, ItemStack> recipes = PopFurnaceRecipes.instance().getPoppingList();
			arecipes.addAll(recipes.entrySet().stream().map(recipe -> new PoppingPair(recipe.getKey(), recipe.getValue())).collect(Collectors.toList()));
		} else
			super.loadCraftingRecipes(outputId, results);
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public void loadCraftingRecipes(ItemStack result) {
		Map<ItemStack, ItemStack> recipes = PopFurnaceRecipes.instance().getPoppingList();
		arecipes.addAll(recipes.entrySet().stream().filter(recipe -> NEIServerUtils.areStacksSameType(recipe.getValue(), result)).map(recipe -> new PoppingPair(recipe.getKey(), recipe.getValue())).collect(Collectors.toList()));
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public void loadUsageRecipes(ItemStack ingredient) {
		Map<ItemStack, ItemStack> recipes = PopFurnaceRecipes.instance().getPoppingList();
		recipes.entrySet().stream().filter(recipe -> NEIServerUtils.areStacksSameTypeCrafting(recipe.getKey(), ingredient)).forEach(recipe -> {
			PoppingPair arecipe = new PoppingPair(recipe.getKey(), recipe.getValue());
			arecipe.setIngredientPermutation(Collections.singletonList(arecipe.isin), ingredient);
			arecipes.add(arecipe);
		});
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public String getOverlayIdentifier() {
		return "popping";
	}
	@Optional.Method(modid = "NotEnoughItems")
	@Override
	public String getGuiTexture() {
		return "unlogicii:textures/gui/nei_pop_furnace.png";
	}
}