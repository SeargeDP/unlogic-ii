package the_fireplace.unlogicii.events;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.AchievementList;
import net.minecraft.world.WorldProviderEnd;
import net.minecraft.world.WorldProviderHell;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.EntityStruckByLightningEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.AchievementEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.living.ExtendedLivingBase;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;

import java.util.Random;
/**
 * @author The_Fireplace
 */
@SuppressWarnings("unused")
public class CommonEvents {
	@SubscribeEvent
	public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
		if(eventArgs.modID.equals(UnLogicII.MODID))
			UnLogicII.instance.syncConfig();
	}
	@SubscribeEvent
	public void onEntityConstructing(EntityConstructing event){
		if(event.entity instanceof EntityPlayer && ExtendedPlayer.get((EntityPlayer)event.entity) == null){
			ExtendedPlayer.register((EntityPlayer)event.entity);
		}
		if(event.entity instanceof EntityLiving && ExtendedLivingBase.get((EntityLivingBase)event.entity) == null && !(event.entity instanceof EntityPlayer)){
			ExtendedLivingBase.register((EntityLiving)event.entity);
		}
	}
	@SubscribeEvent
	public void beforeBreak(BlockEvent.BreakEvent event){
		if(event.getPlayer() != null){
			if(event.getPlayer().capabilities.isCreativeMode){
				if(event.getPlayer().getHeldItem() != null){
					if(event.getPlayer().getHeldItem().getItem().equals(UnLogicII.dark_knight_sword) || event.getPlayer().getHeldItem().getItem().equals(UnLogicII.paladin_sword)){
						event.setCanceled(true);
					}
				}
			}
		}
	}
	@SubscribeEvent
	public void onLivingDeath(LivingDropsEvent event){
		if(ExtendedLivingBase.get(event.entityLiving) != null){
			Random rand = new Random();
			ExtendedLivingBase entity = ExtendedLivingBase.get(event.entityLiving);
			if(event.entityLiving.worldObj.provider instanceof WorldProviderEnd){
				int value = rand.nextInt(50);
				if(value == 1){
					if(entity.getSoulType() == -1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_negative)));
					}else if(entity.getSoulType() == 0){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_neutral)));
					}else if(entity.getSoulType() == 1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_positive)));
					}
				}
			}else if(event.entityLiving.worldObj.provider instanceof WorldProviderHell){
				int value = rand.nextInt(500);
				if(value == 1){
					if(entity.getSoulType() == -1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_negative)));
					}else if(entity.getSoulType() == 0){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_neutral)));
					}else if(entity.getSoulType() == 1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_positive)));
					}
				}
			}else{
				int value = rand.nextInt(1000);
				if(value == 1){
					if(entity.getSoulType() == -1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_negative)));
					}else if(entity.getSoulType() == 0){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_neutral)));
					}else if(entity.getSoulType() == 1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_positive)));
					}
				}
			}
			if(entity.getIsInfected()){
				int value = rand.nextInt(10);
				if(value == 1){
					if(entity.getSoulType() == -1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_negative)));
					}else if(entity.getSoulType() == 0){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_neutral)));
					}else if(entity.getSoulType() == 1){
						event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_positive)));
					}
				}
			}
		}
		if(event.entityLiving instanceof EntityPlayer){
			Random rand = new Random();
			ExtendedPlayer entity = ExtendedPlayer.get((EntityPlayer)event.entityLiving);
			if(rand.nextInt(5) == 1)
				if(entity.getKarma() < 0)
					event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_negative)));
				else if(entity.getKarma() == 0)
					event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_neutral)));
				else if(entity.getKarma() > 0)
					event.drops.add(new EntityItem(event.entityLiving.worldObj, event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, new ItemStack(UnLogicII.soul_positive)));
		}
		if(event.entityLiving instanceof EntityPlayer && event.source.getEntity() instanceof EntityPlayer){
			EntityPlayer attacker = (EntityPlayer)event.source.getEntity();
			EntityPlayer reciever = (EntityPlayer)event.entityLiving;
			if(attacker!=reciever)
				if(ExtendedPlayer.get(attacker).wasAttackedBy(reciever)){
					ExtendedPlayer.get(attacker).removeAttacker(reciever);
					ExtendedPlayer.get(reciever).addToKarma(4);//a little bit of karma came full circle, as the reciever hit the attacker first
				}else{
					ExtendedPlayer.get(attacker).removeFromKarma(50);//Lose a lot of karma for attacking and killing someone
				}
			if(attacker!=reciever)
				if(ExtendedPlayer.get(reciever).getKarma() < ExtendedPlayer.get(attacker).getKarma() && ExtendedPlayer.get(reciever).getKarma() < 0){
					ExtendedPlayer.get(attacker).addToKarma(20);//gain some karma for killing a bad guy that is worse than you
				}
		}
		if(event.entityLiving instanceof EntityMob && event.source.getEntity() instanceof EntityPlayer){
			EntityPlayer attacker = (EntityPlayer)event.source.getEntity();
			ExtendedPlayer.get(attacker).addToKarma(1);//As much as I hate to do it like this, as it allows karma grinders, I don't want to use massive databases
		}
		if(event.entityLiving instanceof EntityAnimal && event.source.getEntity() instanceof EntityPlayer){
			EntityPlayer attacker = (EntityPlayer)event.source.getEntity();
			if(attacker.getFoodStats().needFood()){
				ExtendedPlayer.get(attacker).addToKarma(1);//It isn't so bad, since the player needs food
			}
			ExtendedPlayer.get(attacker).removeFromKarma(2);//but an innocent animal still dies in the process
		}
		if(event.entityLiving instanceof EntityVillager && event.source.getEntity() instanceof EntityPlayer){
			EntityPlayer attacker = (EntityPlayer)event.source.getEntity();
			ExtendedPlayer.get(attacker).removeFromKarma(10);//No murder
		}
	}
	@SubscribeEvent
	public void onLivingHurt(LivingHurtEvent event){
		if(event.entityLiving instanceof EntityPlayer && event.source.getEntity() instanceof EntityPlayer){
			EntityPlayer attacker = (EntityPlayer)event.source.getEntity();
			EntityPlayer reciever = (EntityPlayer)event.entityLiving;
			if(attacker!=reciever)
				if(!ExtendedPlayer.get(attacker).wasAttackedBy(reciever)){
					if(!ExtendedPlayer.get(reciever).wasAttackedBy(attacker))
						ExtendedPlayer.get(reciever).addAttacker(attacker);
					ExtendedPlayer.get(attacker).removeFromKarma(2);//lose karma for attacking someone who didn't attack you first
					if(reciever.getEquipmentInSlot(0) == null)
						ExtendedPlayer.get(attacker).removeFromKarma(9);//lose more karma because the victim was unarmed
				}
		}
	}
	@SubscribeEvent
	public void onAchievementEvent(AchievementEvent event){
		ExtendedPlayer player = ExtendedPlayer.get(event.entityPlayer);
		if(event.achievement == AchievementList.breedCow){
			player.addToKarma(20);//Earns karma for breeding a cow for the first time
		}
		if(event.achievement == AchievementList.diamondsToYou){
			player.addToKarma(100);//earns karma for the player's generosity
		}
		if(event.achievement == AchievementList.killEnemy){
			player.addToKarma(10);//earns karma for their first kill of something evil
		}
		if(event.achievement == AchievementList.bakeCake){
			player.addToKarma(50);//earns karma because making a cake is usually a nice thing to do
		}
		if(event.achievement == AchievementList.killWither){
			player.addToKarma(150);//earns karma for killing an evil boss
		}
		if(event.achievement == AchievementList.snipeSkeleton){
			player.addToKarma(10);//earns a bit of karma for another killing evil achievement
		}
		if(event.achievement == AchievementList.ghast){
			player.addToKarma(15);//earns a bit of karma for another killing evil achievement
		}
		if(event.achievement == AchievementList.spawnWither){
			player.removeFromKarma(50);//loses karma for making a monster, will be regained if you kill it
		}
		if(event.achievement == AchievementList.buildHoe){
			player.addToKarma(25);//earns karma for preparing to make a renewable food source
		}
		if(event.achievement == AchievementList.theEnd2){
			player.addToKarma(100);//earns karma for killing an evil boss
		}
	}
	@SubscribeEvent
	public void onLightningEvent(EntityStruckByLightningEvent event){
		if(event.entity instanceof EntityPlayer){
			if(ExtendedPlayer.get((EntityPlayer)event.entity).getKarma() < 0){//Getting struck by lightning is bad karma coming back around.
				if(ExtendedPlayer.get((EntityPlayer)event.entity).getKarma() <= -25){
					ExtendedPlayer.get((EntityPlayer)event.entity).addToKarma(25);
				}else{
					ExtendedPlayer.get((EntityPlayer)event.entity).addToKarma(Math.abs(ExtendedPlayer.get((EntityPlayer)event.entity).getKarma()));
				}
			}
		}
	}
	@SubscribeEvent
	public void onLivingUpdate(LivingEvent.LivingUpdateEvent event){
		if(!(event.entityLiving instanceof EntityPlayer) || event.entityLiving.worldObj.isRemote || !(event.entityLiving.isPotionActive(UnLogicII.hallucination))) {
			if(event.entityLiving instanceof EntityPlayer && !(event.entityLiving.isPotionActive(UnLogicII.hallucination))){
				UnLogicII.proxy.tryRemoveShader();
			}
		}
		else
			UnLogicII.hallucination.performEffect(event.entityLiving, 0);
	}
}
