package the_fireplace.unlogicii.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;
import the_fireplace.unlogicii.enums.EnumAmmo;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ChangeAmmoMessage implements IMessage {

	public ChangeAmmoMessage(){}

	@Override
	public void fromBytes(ByteBuf buf) {

	}

	@Override
	public void toBytes(ByteBuf buf) {

	}

	public static class Handler extends AbstractServerMessageHandler<ChangeAmmoMessage> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, ChangeAmmoMessage message, MessageContext ctx) {
			EnumAmmo ammo = ExtendedPlayer.get(player).getAmmoType();
			ExtendedPlayer.get(player).setAmmo(ammo.next());
			return new SetAmmoMessage(ExtendedPlayer.get(player).getAmmoType());
		}

	}
}
