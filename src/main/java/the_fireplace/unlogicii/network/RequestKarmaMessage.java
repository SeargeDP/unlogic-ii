package the_fireplace.unlogicii.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;

public class RequestKarmaMessage implements IMessage {

	public RequestKarmaMessage(){}

	@Override
	public void fromBytes(ByteBuf buf) {

	}

	@Override
	public void toBytes(ByteBuf buf) {

	}

	public static class Handler extends AbstractServerMessageHandler<RequestKarmaMessage> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, RequestKarmaMessage message, MessageContext ctx) {
			long karma = ExtendedPlayer.get(player).getKarma();
			return new KarmaMessage(karma);
		}

	}
}
