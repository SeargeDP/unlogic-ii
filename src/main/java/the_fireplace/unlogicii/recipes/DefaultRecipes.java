package the_fireplace.unlogicii.recipes;

import the_fireplace.unlogicii.api.PopFurnaceRegistry;
import the_fireplace.unlogicii.libs.tools.MIDLib;
/**
 * 
 * @author The_Fireplace
 *
 */
public class DefaultRecipes extends VanillaStacks implements IRecipeRegister {
	@Override
	public void registerRecipes() {
		if(!MIDLib.hasBaseMetals()){//in any mod adding gold dust, replace with the appropriate amount of gold dust instead of nuggets
			PopFurnaceRegistry.registerPopFurnaceRecipe(goldenAppleStack, goldIngotStack, 8);
			PopFurnaceRegistry.registerPopFurnaceRecipe(glisteringMelonStack, goldNuggetStack, 8);
			PopFurnaceRegistry.registerPopFurnaceRecipe(goldenCarrotStack, goldNuggetStack, 8);
		}
		if(!MIDLib.hasBaseMetals() && !MIDLib.hasSteelIndustries()){//mods adding different metals/materials may change this
			shaped(darkKnightSwordStack, " gd", "gig", "sg ", 'g', unlogicGemNegativeStack, 'd', diamondStack, 's', "stickWood", 'i', goldIngotStack);
			shaped(paladinSwordStack, " gd", "gig", "sg ", 'g', unlogicGemPositiveStack, 'd', diamondStack, 's', "stickWood", 'i', goldIngotStack);
		}
		if(!MIDLib.hasPowerAdvantage()){//Machine Frames can be used
			shaped(shellCoreStack, "grg", "rer", "grg", 'g', goldBlockStack, 'r', redstoneBlockStack, 'e', enderChestStack);
		}
		if(!MIDLib.hasPowerAdvantage()){//Mods adding machines may have components what work better as parts of this machine
			shaped(popFurnaceStack, "iii", "ifi", "ici", 'i', ironStack, 'f', furnaceStack, 'c', cauldronStack);
		}
	}
}
