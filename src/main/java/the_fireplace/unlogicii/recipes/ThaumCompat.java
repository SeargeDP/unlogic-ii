package the_fireplace.unlogicii.recipes;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import thaumcraft.api.ThaumcraftApi;
import thaumcraft.api.aspects.Aspect;
import thaumcraft.api.aspects.AspectList;
import thaumcraft.api.blocks.BlocksTC;
import thaumcraft.api.items.ItemsTC;
import the_fireplace.unlogicii.api.PopFurnaceRegistry;

/**
 * @author The_Fireplace
 */
public class ThaumCompat extends VanillaStacks implements IRecipeRegister {
	@Override
	public void registerRecipes(){
		ItemStack orderShardStack = new ItemStack(ItemsTC.shard, 1, 4);
		ItemStack entropyShardStack = new ItemStack(ItemsTC.shard, 1, 5);
		ItemStack nuggetChicken = new ItemStack(ItemsTC.nuggets, 1, 1);
		ItemStack nuggetPork = new ItemStack(ItemsTC.nuggets, 1, 2);
		ItemStack nuggetBeef = new ItemStack(ItemsTC.nuggets, 1, 0);
		ItemStack nuggetMutton = new ItemStack(ItemsTC.nuggets, 1, 5);
		ItemStack nuggetFish = new ItemStack(ItemsTC.nuggets, 1, 3);
		ItemStack nuggetRabbit = new ItemStack(ItemsTC.nuggets, 1, 4);
		ItemStack alumentum = new ItemStack(ItemsTC.alumentum);
		ItemStack nitor = new ItemStack(BlocksTC.nitor);

		ThaumcraftApi.registerObjectTag(soulNegativeStack, new AspectList().add(Aspect.SOUL, 3).add(Aspect.DARKNESS, 1));
		ThaumcraftApi.registerObjectTag(soulNeutralStack, new AspectList().add(Aspect.SOUL, 4));
		ThaumcraftApi.registerObjectTag(soulPositiveStack, new AspectList().add(Aspect.SOUL, 3).add(Aspect.ORDER, 1));
		ThaumcraftApi.registerObjectTag("screen", new AspectList().add(Aspect.CRAFT, 2).add(Aspect.SENSES, 1));
		ThaumcraftApi.registerObjectTag(blazeCakeStack, new AspectList().add(Aspect.FIRE, 3).add(Aspect.ENERGY, 1).add(Aspect.LIFE, 4).add(Aspect.DESIRE, 1));
		ThaumcraftApi.registerObjectTag(chargedCoalBlockStack, new AspectList().add(Aspect.FIRE, 13).add(Aspect.ENERGY, 24));
		ThaumcraftApi.registerObjectTag(compactDirtStack, new AspectList().add(Aspect.EARTH, 12));
		ThaumcraftApi.registerObjectTag(fireplaceBottomStack, new AspectList().add(Aspect.EARTH, 3).add(Aspect.FIRE, 1));
		ThaumcraftApi.registerObjectTag(polishedStoneStack, new AspectList().add(Aspect.EARTH, 2));
		ThaumcraftApi.registerObjectTag(popFurnaceStack, new AspectList().add(Aspect.ENTROPY, 8).add(Aspect.METAL, 8).add(Aspect.VOID, 2).add(Aspect.FIRE, 1));
		ThaumcraftApi.registerObjectTag(chargedCoalStack, new AspectList().add(Aspect.ENERGY, 4).add(Aspect.FIRE, 2).add(Aspect.ENTROPY, 1));
		ThaumcraftApi.registerObjectTag(crystalEyeStack, new AspectList().add(Aspect.CRYSTAL, 4).add(Aspect.ENERGY, 2).add(Aspect.DESIRE, 4).add(Aspect.SENSES, 4));
		ThaumcraftApi.registerObjectTag(crystalEyeHeadbandStack, new AspectList().add(Aspect.CRYSTAL, 4).add(Aspect.ENERGY, 2).add(Aspect.DESIRE, 4).add(Aspect.SENSES, 4).add(Aspect.PROTECT, 2).add(Aspect.ELDRITCH, 1));
		ThaumcraftApi.registerObjectTag(darkKnightSwordStack, new AspectList().add(Aspect.DARKNESS, 4).add(Aspect.AVERSION, 4).add(Aspect.METAL, 2).add(Aspect.CRYSTAL, 6).add(Aspect.DESIRE, 2));
		ThaumcraftApi.registerObjectTag(paladinSwordStack, new AspectList().add(Aspect.ORDER, 4).add(Aspect.AVERSION, 4).add(Aspect.METAL, 2).add(Aspect.CRYSTAL, 6).add(Aspect.DESIRE, 2));
		ThaumcraftApi.registerObjectTag(destabilizedCoalStack, new AspectList().add(Aspect.ENTROPY, 4).add(Aspect.ENERGY, 4).add(Aspect.FIRE, 3));
		ThaumcraftApi.registerObjectTag(firestarterSubstituteStack, new AspectList().add(Aspect.FIRE, 4).add(Aspect.EXCHANGE, 1));
		ThaumcraftApi.registerObjectTag(gunpowderSubstituteStack, new AspectList().add(Aspect.FIRE, 2).add(Aspect.ENTROPY, 4).add(Aspect.EXCHANGE, 1));
		ThaumcraftApi.registerObjectTag(liquidUnlogicGemNegativeStackC, new AspectList().add(Aspect.SOUL, 1).add(Aspect.DARKNESS, 3).add(Aspect.EXCHANGE, 2));
		ThaumcraftApi.registerObjectTag(liquidUnlogicGemNeutralStackC, new AspectList().add(Aspect.SOUL, 1).add(Aspect.EXCHANGE, 1).add(Aspect.VOID, 1));
		ThaumcraftApi.registerObjectTag(liquidUnlogicGemPositiveStackC, new AspectList().add(Aspect.SOUL, 1).add(Aspect.ORDER, 3).add(Aspect.EXCHANGE, 2));
		ThaumcraftApi.registerObjectTag(obsidianToolStack, new AspectList().add(Aspect.TOOL, 4).add(Aspect.CRYSTAL, 8).add(Aspect.DESIRE, 2));
		ThaumcraftApi.registerObjectTag(quartzSeedsStack, new AspectList().add(Aspect.CRYSTAL, 4).add(Aspect.ENERGY, 2).add(Aspect.PLANT, 4));
		ThaumcraftApi.registerObjectTag(refinedCoalStack, new AspectList().add(Aspect.FIRE, 9).add(Aspect.ENERGY, 4));
		ThaumcraftApi.registerObjectTag(restabilizedCoalStack, new AspectList().add(Aspect.FIRE, 6).add(Aspect.ENERGY, 4).add(Aspect.ENTROPY, 1));
		ThaumcraftApi.registerObjectTag(smartCoalGunStack, new AspectList().add(Aspect.TOOL, 9).add(Aspect.METAL, 9).add(Aspect.DESIRE, 6).add(Aspect.FIRE, 1).add(Aspect.MIND, 1));
		ThaumcraftApi.registerObjectTag(soulNegativeStack, new AspectList().add(Aspect.CRYSTAL, 3).add(Aspect.DARKNESS, 4).add(Aspect.DESIRE, 1));
		ThaumcraftApi.registerObjectTag(soulNeutralStack, new AspectList().add(Aspect.CRYSTAL, 3).add(Aspect.DESIRE, 1));
		ThaumcraftApi.registerObjectTag(soulPositiveStack, new AspectList().add(Aspect.CRYSTAL, 3).add(Aspect.ORDER, 4).add(Aspect.DESIRE, 1));
		ThaumcraftApi.registerObjectTag(wabbajackStack, new AspectList().add(Aspect.CRYSTAL, 9).add(Aspect.DESIRE, 6).add(Aspect.ENTROPY, 6).add(Aspect.ENERGY, 4).add(Aspect.MECHANISM, 3).add(Aspect.SENSES, 1).add(Aspect.FIRE, 1).add(Aspect.AVERSION, 1).add(Aspect.TOOL, 1).add(Aspect.ELDRITCH, 1));
		ThaumcraftApi.registerObjectTag(fossilStack, new AspectList().add(Aspect.EARTH, 2).add(Aspect.DEATH, 2));
		ThaumcraftApi.registerObjectTag(obsidianFarmlandStack, new AspectList().add(Aspect.EARTH, 2).add(Aspect.FIRE, 2).add(Aspect.DARKNESS, 1));
		OreDictionary.registerOre("book", ItemsTC.thaumonomicon);
		OreDictionary.registerOre("book", ItemsTC.crimsonRites);
		shapeless(orderShardStack, entropyShardStack, liquidUnlogicGemPositiveStackC);
		shapeless(entropyShardStack, orderShardStack, liquidUnlogicGemNegativeStackC);
		PopFurnaceRegistry.registerPopFurnaceRecipe(beefStack, nuggetBeef, 8);
		PopFurnaceRegistry.registerPopFurnaceRecipe(chickenStack, nuggetChicken, 6);
		PopFurnaceRegistry.registerPopFurnaceRecipe(fishStack, nuggetFish, 5);
		PopFurnaceRegistry.registerPopFurnaceRecipe(muttonStack, nuggetMutton, 6);
		PopFurnaceRegistry.registerPopFurnaceRecipe(porkStack, nuggetPork, 8);
		PopFurnaceRegistry.registerPopFurnaceRecipe(rabbitStack, nuggetRabbit, 5);
		PopFurnaceRegistry.registerFirestarter(nitor);
		PopFurnaceRegistry.registerGunpowder(alumentum);
	}
}
