package the_fireplace.unlogicii.recipes;

import cyano.poweradvantage.init.Blocks;
import cyano.poweradvantage.init.Items;
import net.minecraft.item.ItemStack;
import the_fireplace.unlogicii.api.PopFurnaceRegistry;

public class PowerAdvantageRecipes extends VanillaStacks implements IRecipeRegister {
	ItemStack steelFrameStack = new ItemStack(Blocks.steel_frame);
	ItemStack starchStack = new ItemStack(Items.starch);
	@Override
	public void registerRecipes() {
		shaped(shellCoreStack, "grg", "rfr", "grg", 'g', goldBlockStack, 'r', redstoneBlockStack, 'f', steelFrameStack);
		shaped(popFurnaceStack, " i ", "ifi", " c ", 'i', ironStack, 'f', steelFrameStack, 'c', cauldronStack);
		//Mod only recipes
		PopFurnaceRegistry.registerPopFurnaceRecipe(potatoStack, starchStack, 1);
		PopFurnaceRegistry.registerPopFurnaceRecipe(poisonousPotatoStack, starchStack, 1);
	}
}
