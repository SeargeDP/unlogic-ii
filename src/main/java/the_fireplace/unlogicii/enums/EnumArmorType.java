package the_fireplace.unlogicii.enums;
/**
 * 
 * @author The_Fireplace
 *
 */
public enum EnumArmorType {
	HELMET,
	CHESTPLATE,
	LEGGINGS,
	BOOTS;
	public int getIndex(){
		if(this == HELMET){
			return 0;
		}else if(this == CHESTPLATE){
			return 1;
		}else if(this == LEGGINGS){
			return 2;
		}else if(this == BOOTS){
			return 3;
		}else{
			System.out.println("Error: EnumArmorType "+this.toString()+" does not exist. Returning something out of bounds.");
			return -1;
		}
	} 
}
