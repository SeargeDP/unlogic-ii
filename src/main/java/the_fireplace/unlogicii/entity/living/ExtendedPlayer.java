package the_fireplace.unlogicii.entity.living;

import java.util.ArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import the_fireplace.unlogicii.enums.EnumAmmo;
/**
 *
 * @author The_Fireplace
 *
 */
public class ExtendedPlayer implements IExtendedEntityProperties {
	//Every instance of IExtendedEntityProperties needs this
	public final static String EXT_PROP_NAME = "UnLogicIIExtendedPlayer";
	//This shows which entity the class is extending. Not sure if necessary, but adding it for completeness
	private final EntityPlayer player;

	private EnumAmmo ammo;
	private long karma;
	private ArrayList attackers;

	public ExtendedPlayer(EntityPlayer player){
		this.player = player;//Initializes Player

		this.ammo = EnumAmmo.COAL;
		this.karma = 0;
		this.attackers = new ArrayList();
	}
	//Registers Extended Properties
	public static final void register(EntityPlayer player){
		player.registerExtendedProperties(ExtendedPlayer.EXT_PROP_NAME, new ExtendedPlayer(player));
	}
	//Returns properties
	public static final ExtendedPlayer get(EntityPlayer player){
		return (ExtendedPlayer)player.getExtendedProperties(EXT_PROP_NAME);
	}

	@Override
	public void saveNBTData(NBTTagCompound compound) {
		//New compound that will save the properties
		NBTTagCompound properties = new NBTTagCompound();
		//Save the data here
		properties.setString("AmmoType", EnumAmmo.getStringFromAmmo(this.ammo));
		properties.setLong("Karma", this.karma);
		saveStringArray(properties, attackers, "Attackers");
		//Sets the unique tag name
		compound.setTag(EXT_PROP_NAME, properties);
	}

	@Override
	public void loadNBTData(NBTTagCompound compound) {
		//Fetches the tag
		NBTTagCompound properties = (NBTTagCompound)
				compound.getTag(EXT_PROP_NAME);
		//get data from the custom tag compound
		this.ammo = EnumAmmo.getAmmoFromString(properties.getString("AmmoType"));
		this.karma = properties.getInteger("Karma");
		loadStringArray(properties, attackers, "Attackers");
	}

	private void saveStringArray(NBTTagCompound properties, ArrayList list, String listname){
		int i = 0;
		while(i < list.size()){
			properties.setString(listname+String.valueOf(i), (String)list.get(i));
			i++;
		}
	}

	private void loadStringArray(NBTTagCompound properties, ArrayList list, String listname){
		int i = 0;
		while(i < list.size()){
			list.add(properties.getString(listname+String.valueOf(i)));
			i++;
		}
	}

	@Override
	public void init(Entity entity, World world) {}

	public EnumAmmo getAmmoType(){
		return ammo;
	}

	public void setAmmo(EnumAmmo ammo){
		this.ammo = ammo;
	}
	public long getKarma(){
		return karma;
	}

	public void addToKarma(long amount){
		karma += amount;
	}

	public void removeFromKarma(long amount){
		karma -= amount;
	}

	public void setKarmaTo(long amount){
		karma = amount;
	}

	public boolean wasAttackedBy(EntityPlayer player){
		return attackers.contains(player.getUniqueID().toString());
	}

	public void removeAttacker(EntityPlayer player){
		if(attackers.contains(player.getUniqueID().toString())){
			attackers.remove(player.getUniqueID().toString());
		}
	}

	public void addAttacker(EntityPlayer player){
		if(!attackers.contains(player.getUniqueID().toString())){
			attackers.add(player.getUniqueID().toString());
		}
	}
}
