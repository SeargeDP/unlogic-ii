package the_fireplace.unlogicii.config;
/**
 *
 * @author The_Fireplace
 *
 */
public class ConfigValues {
	public static final boolean ENABLESHELL_DEFAULT = true;
	public static boolean ENABLESHELL;
	public static final String ENABLESHELL_NAME = "cfg.enable_shell";

	public static final boolean ENABLEDAMAGE_DEFAULT = true;
	public static boolean ENABLEDAMAGE;
	public static final String ENABLEDAMAGE_NAME = "cfg.enable_block_damage";

	public static final int ITEMSPERGUNPOWDER_DEFAULT = 5;
	public static int ITEMSPERGUNPOWDER;
	public static final String ITEMSPERGUNPOWDER_NAME = "cfg.items_per_gunpowder";

	public static final boolean ENABLEFOSSILGEN_DEFAULT = true;
	public static boolean ENABLEFOSSILGEN;
	public static final String ENABLEFOSSILGEN_NAME = "cfg.enable_fossil_gen";

	public static final int POTIONSWITCH_DEFAULT = 2;
	public static int POTIONSWITCH;
	public static final String POTIONSWITCH_NAME = "cfg.potion_switch";

	public static final boolean ENABLESSS_DEFAULT = false;
	public static boolean ENABLESSS;
	public static final String ENABLESSS_NAME = "cfg.enable_sss";
}
