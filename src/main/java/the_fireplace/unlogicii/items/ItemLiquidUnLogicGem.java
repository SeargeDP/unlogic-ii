package the_fireplace.unlogicii.items;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.living.ExtendedLivingBase;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ItemLiquidUnLogicGem extends Item {
	String type;
	public ItemLiquidUnLogicGem(String type){
		super();
		setCreativeTab(UnLogicII.TabUnLogicII);
		setMaxStackSize(1);
		setMaxDamage(16);
		setNoRepair();
		setUnlocalizedName("liquid_unlogic_gem_"+type);
		this.type=type;
	}
	@Override
	public boolean hasContainerItem(ItemStack stack){
		return true;
	}
	@Override
	public ItemStack getContainerItem(ItemStack is){
		is.setItemDamage(is.getItemDamage() + 1);
		return is;
	}
	@Override
	public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer playerIn, EntityLivingBase target)
	{
		if(ExtendedLivingBase.get(target) != null){
			ExtendedLivingBase tgt = ExtendedLivingBase.get(target);
			switch (this.type) {
				case "negative":
					tgt.setSoulType((byte) -1);
					break;
				case "neutral":
					tgt.setSoulType((byte) 0);
					break;
				case "positive":
					tgt.setSoulType((byte) 1);
					break;
				default:
					System.out.println("ERROR: Unknown Liquid UnLogic Crystal Type: " + this.type);
					return false;
			}
			tgt.setIsInfected(true);
			stack.damageItem(1, playerIn);
			return true;
		}
		return false;
	}
}
