package the_fireplace.unlogicii.items;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import the_fireplace.unlogicii.UnLogicII;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ItemObsidianTool extends Item {
	public ItemObsidianTool(){
		setMaxStackSize(1);
		setUnlocalizedName("obsidian_tool");
		setCreativeTab(UnLogicII.TabUnLogicII);
		setMaxDamage(1562);//same as diamond pickaxe
		setFull3D();
	}
	@Override
	public boolean hitEntity(ItemStack is, EntityLivingBase entityHit, EntityLivingBase attacker){
		is.damageItem(2, attacker);
		return true;
	}
	@Override
	public boolean onBlockDestroyed(ItemStack is, World world, Block block, BlockPos pos, EntityLivingBase player)
	{
		if(block.getBlockHardness(world, pos) != 0.0D){
			is.damageItem(1, player);
		}
		if(block == Blocks.obsidian || block == UnLogicII.obsidian_farmland){
			block.dropBlockAsItem(world, pos, block.getDefaultState(), 0);
		}
		return true;
	}
	@Override
	public int getItemEnchantability(){
		return 10;
	}
	@Override
	public float getDigSpeed(ItemStack itemstack, IBlockState state)
	{
		if(state.getBlock() == Blocks.obsidian || state.getBlock() == UnLogicII.obsidian_farmland){
			return 5000;
		}
		return super.getDigSpeed(itemstack, state);
	}
	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(worldIn.getBlockState(pos).getBlock() == Blocks.obsidian){
			worldIn.setBlockToAir(pos);
			worldIn.setBlockState(pos, UnLogicII.obsidian_farmland.getDefaultState());
			stack.damageItem(1, playerIn);
			return true;
		}
		return false;
	}
}
