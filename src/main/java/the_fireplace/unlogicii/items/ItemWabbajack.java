package the_fireplace.unlogicii.items;

import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityEnderPearl;
import net.minecraft.entity.item.EntityExpBottle;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ItemWabbajack extends Item {
	public ItemWabbajack(){
		setUnlocalizedName("wabbajack");
		setCreativeTab(UnLogicII.TabUnLogicII);
		setMaxStackSize(1);
		setMaxDamage(128);
	}
	@Override
	public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player){
		Random rand = new Random();
		int r = rand.nextInt(100);
		if(ExtendedPlayer.get(player).getKarma() > 0){
			activateHelpful(is, world, player);
		}
		if(ExtendedPlayer.get(player).getKarma() < 0){
			activateHarmful(is, world, player);
		}
		if(ExtendedPlayer.get(player).getKarma() == 0){
			/*if(r >= 0 && r <50 ){
				//return one of the books
			}else */if(r >= 0 && r < 50){
				activateHarmful(is, world, player);
			}else{
				activateHelpful(is, world, player);
			}
		}
		return is;
	}

	public void activateHarmful(ItemStack is, World world, EntityPlayer player){
		ExtendedPlayer player2 = ExtendedPlayer.get(player);
		Random rand = new Random();
		int r = rand.nextInt(100);
		int q = rand.nextInt(3) - 1;
		if(r >= 0 && r < 20){
			if(!world.isRemote){
				world.spawnEntityInWorld(new EntityEnderPearl(world, player));
			}
			player2.addToKarma(1);
		}
		if(r >= 20 && r < 40){
			world.spawnEntityInWorld(new EntityLightningBolt(world, player.posX, player.posY, player.posZ));
			player2.addToKarma(3);
		}
		if(r >= 40 && r < 60){
			for(int h=-3;h < 4;h++){
				for(int g=-3;g < 4;g++){
					for(int j=-3;j < 4;j++){
						if(world.getBlockState(new BlockPos(h, g, j)) == Blocks.stone.getDefaultState()){
							world.setBlockToAir(new BlockPos(h, g, j));
							world.setBlockState(new BlockPos(h, g, j), Blocks.monster_egg.getDefaultState());
							player2.addToKarma(1);
						}
					}
				}
			}
		}
		if(r >= 60 && r < 80){
			if(world.isRemote){
				Minecraft.getMinecraft().entityRenderer.activateNextShader();
			}
		}
		if(r >= 80 && r < 90){
			if(world.getDifficulty() != EnumDifficulty.PEACEFUL){
				if(!world.isRemote){
					EntityCreeper creeper = new EntityCreeper(world);
					creeper.posX = player.posX;
					creeper.posY = player.posY;
					creeper.posZ = player.posZ;
					creeper.setCreeperState(1);
					world.spawnEntityInWorld(creeper);
					if(player2.getKarma() < -100)
						world.spawnEntityInWorld(new EntityLightningBolt(world, creeper.posX, creeper.posY, creeper.posZ));
					player2.addToKarma(10);
				}
			}else{
				world.spawnEntityInWorld(new EntityLightningBolt(world, player.posX, player.posY, player.posZ));
				player2.addToKarma(10);
			}
		}
		if(r >= 90 && r < 95){
			if(!world.isRemote){
				world.spawnEntityInWorld(new EntityLargeFireball(world, player, player.posX+r*q, player.posY+q, player.posZ+r*q));
			}
		}
		if(r >= 95 && r < 100){
			activateHelpful(is, world, player);
		}
		is.damageItem(1, player);
	}

	public void activateHelpful(ItemStack is, World world, EntityPlayer player){
		ExtendedPlayer player2 = ExtendedPlayer.get(player);
		Random rand = new Random();
		int r = rand.nextInt(100);
		if(r >= 0 && r < 20){
			if(!world.isRemote){
				world.spawnEntityInWorld(new EntityLargeFireball(world, player, player.posX, player.posY, player.posZ));
			}
		}
		if(r >= 20 && r < 40){
			if(!world.isRemote){
				world.spawnEntityInWorld(new EntityExpBottle(world, player));
			}
		}
		if(r >= 40 && r < 60){
			if(player.getFoodStats().needFood()){
				player.getFoodStats().setFoodLevel(40);
				player2.removeFromKarma(1);
			}else if(player.getHealth() < player.getMaxHealth()){
				player2.removeFromKarma((int)player.getMaxHealth() - (int)player.getHealth());
				player.heal(player.getMaxHealth() - player.getHealth());
			}else{
				player.addScore(10);
			}
		}
		if(r >= 60 && r < 80){
			if(player.isBurning()){
				player.extinguish();
				player2.removeFromKarma(2);
			}else{
				if(!world.isRemote)
					player.fireResistance += 10;
				player2.removeFromKarma(10);
			}
		}
		if(r >= 80 && r < 90){
			for(int h=-3;h < 4;h++){
				for(int g=-3;g < 4;g++){
					for(int j=-3;j < 4;j++){
						if(world.getBlockState(new BlockPos(h, g, j)) == Blocks.stone.getDefaultState()){
							world.setBlockToAir(new BlockPos(h, g, j));
							world.setBlockState(new BlockPos(h, g, j), Blocks.gold_ore.getDefaultState());
							player2.removeFromKarma(10);
						}
					}
				}
			}
		}
		if(r >= 90 && r < 95){
			if(!world.isRemote){
				if(is.getItemDamage() < is.getMaxDamage()){
					is.damageItem(-2, player);
				}else{
					world.spawnEntityInWorld(new EntityEgg(world, player));
				}
			}
		}
		if(r >= 95 && r < 100){
			activateHarmful(is, world, player);
		}
		is.damageItem(1, player);
	}
}
