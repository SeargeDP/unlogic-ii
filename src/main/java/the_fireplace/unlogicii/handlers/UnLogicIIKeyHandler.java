package the_fireplace.unlogicii.handlers;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.input.Keyboard;

import the_fireplace.unlogicii.network.ChangeAmmoMessage;
import the_fireplace.unlogicii.network.PacketDispatcher;
/**
 * 
 * @author The_Fireplace
 *
 */
public class UnLogicIIKeyHandler {
	public static final int TOGGLEAMMO = 0;
	private static final String[] desc = {"key.toggleammo.desc"};
	private static final int[] keyValues = {Keyboard.KEY_R};
	private final KeyBinding[] keys;
	public UnLogicIIKeyHandler(){
		keys = new KeyBinding[desc.length];
		for(int i = 0; i < desc.length; ++i){
			keys[i] = new KeyBinding(desc[i], keyValues[i], "key.unlogicii.category");
			ClientRegistry.registerKeyBinding(keys[i]);
		}
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onKeyInput(KeyInputEvent event){
		if(keys[TOGGLEAMMO].isPressed()){
			PacketDispatcher.sendToServer(new ChangeAmmoMessage());
		}
	}
}
