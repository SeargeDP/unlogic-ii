package the_fireplace.unlogicii.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import the_fireplace.unlogicii.entity.EntityHallucinationPotion;
import the_fireplace.unlogicii.entity.coal.*;
import the_fireplace.unlogicii.enums.EnumAmmo;
import the_fireplace.unlogicii.handlers.UnLogicIIKeyHandler;
import the_fireplace.unlogicii.renderers.RenderCoal;
import the_fireplace.unlogicii.renderers.RenderHallucinationPotion;

/**
 *
 * @author The_Fireplace
 *
 */
public class ClientProxy extends CommonProxy {
	@Override
	public void registerEntityRenderers(){

	}
	@Override
	public void registerRenderers(){
		RenderingRegistry.registerEntityRenderingHandler(EntityCoal.class, new RenderCoal(Minecraft.getMinecraft().getRenderManager(), EnumAmmo.COAL));
		RenderingRegistry.registerEntityRenderingHandler(EntityChargedCoal.class, new RenderCoal(Minecraft.getMinecraft().getRenderManager(), EnumAmmo.CHARGED_COAL));
		RenderingRegistry.registerEntityRenderingHandler(EntityDestabilizedCoal.class, new RenderCoal(Minecraft.getMinecraft().getRenderManager(), EnumAmmo.DESTABILIZED_COAL));
		RenderingRegistry.registerEntityRenderingHandler(EntityRestabilizedCoal.class, new RenderCoal(Minecraft.getMinecraft().getRenderManager(), EnumAmmo.RESTABILIZED_COAL));
		RenderingRegistry.registerEntityRenderingHandler(EntityRefinedCoal.class, new RenderCoal(Minecraft.getMinecraft().getRenderManager(), EnumAmmo.REFINED_COAL));
		RenderingRegistry.registerEntityRenderingHandler(EntityHallucinationPotion.class, new RenderHallucinationPotion(Minecraft.getMinecraft().getRenderManager()));
	}

	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx){
		return (ctx.side.isClient() ? Minecraft.getMinecraft().thePlayer : super.getPlayerEntity(ctx));
	}

	@Override
	public void registerClient(){
		MinecraftForge.EVENT_BUS.register(new UnLogicIIKeyHandler());
	}

	@Override
	public void tryRemoveShader(){
		while(Minecraft.getMinecraft().entityRenderer.isShaderActive())
			Minecraft.getMinecraft().entityRenderer.activateNextShader();
	}
}
