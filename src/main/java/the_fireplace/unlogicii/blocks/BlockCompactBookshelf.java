package the_fireplace.unlogicii.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockCompactBookshelf extends ULBlock {

	public BlockCompactBookshelf() {
		super(Material.wood);
		setUnlocalizedName("compact_bookshelf");
		setHardness(4.5F);
	}
	@Override
	public float getEnchantPowerBonus(World world, BlockPos pos)
	{
		return 3;
	}
}
