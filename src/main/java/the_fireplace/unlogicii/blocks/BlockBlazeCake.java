package the_fireplace.unlogicii.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;

import java.util.Random;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockBlazeCake extends ULBlock {
	public static final PropertyInteger BITES = PropertyInteger.create("bites", 0, 6);

	public BlockBlazeCake()
	{
		super(Material.cake);
		setDefaultState(this.blockState.getBaseState().withProperty(BITES, 0));
		setTickRandomly(true);
		setUnlocalizedName("blaze_cake");
		setHardness(0.5F);
		disableStats();
	}
	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess worldIn, BlockPos pos)
	{
		float f = 0.0625F;
		float f1 = (1 + worldIn.getBlockState(pos).getValue(BITES) * 2) / 16.0F;
		float f2 = 0.5F;
		this.setBlockBounds(f1, 0.0F, f, 1.0F - f, f2, 1.0F - f);
	}
	@Override
	public void setBlockBoundsForItemRender()
	{
		float f = 0.0625F;
		float f1 = 0.5F;
		this.setBlockBounds(f, 0.0F, f, 1.0F - f, f1, 1.0F - f);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox(World worldIn, BlockPos pos, IBlockState state)
	{
		float f = 0.0625F;
		float f1 = (1 + state.getValue(BITES) * 2) / 16.0F;
		float f2 = 0.5F;
		return new AxisAlignedBB(pos.getX() + f1, pos.getY(), pos.getZ() + f, pos.getX() + 1 - f, pos.getY() + f2, pos.getZ() + 1 - f);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getSelectedBoundingBox(World worldIn, BlockPos pos)
	{
		return this.getCollisionBoundingBox(worldIn, pos, worldIn.getBlockState(pos));
	}

	@Override
	public boolean isFullCube()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		this.eatCake(worldIn, pos, state, playerIn);
		return true;
	}

	@Override
	public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn)
	{
		this.eatCake(worldIn, pos, worldIn.getBlockState(pos), playerIn);
	}

	private void eatCake(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player)
	{
		if (player.canEat(false))
		{
			player.getFoodStats().addStats(2, 0.1F);
			int i = state.getValue(BITES);

			if (i < 6)
			{
				worldIn.setBlockState(pos, state.withProperty(BITES, i + 1), 3);
			}
			else
			{
				worldIn.setBlockToAir(pos);
			}
			player.addPotionEffect(new PotionEffect(Potion.fireResistance.getId(), 1800));
		}
		if(player.canEat(true)){
			player.getFoodStats().addStats(-6, 0.0F);
			int i = state.getValue(BITES);

			if (i < 6)
			{
				worldIn.setBlockState(pos, state.withProperty(BITES, i + 1), 3);
			}
			else
			{
				worldIn.setBlockToAir(pos);
			}
			player.addPotionEffect(new PotionEffect(Potion.fireResistance.getId(), 4800));
			player.setFire(240);
		}
	}

	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		return super.canPlaceBlockAt(worldIn, pos) && this.canBlockStay(worldIn, pos);
	}

	@Override
	public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock)
	{
		if (!this.canBlockStay(worldIn, pos))
		{
			worldIn.setBlockToAir(pos);
		}
	}

	private boolean canBlockStay(World worldIn, BlockPos pos)
	{
		return worldIn.getBlockState(pos.down()).getBlock().getMaterial().isSolid();
	}

	@Override
	public int quantityDropped(Random random)
	{
		return 1;
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		if(state == this.getDefaultState()){
			return Item.getItemFromBlock(UnLogicII.blaze_cake);
		}else if(state == this.blockState.getBaseState().withProperty(BITES, 1) || state == this.blockState.getBaseState().withProperty(BITES, 2)){
			return Items.magma_cream;
		}else if(state == this.blockState.getBaseState().withProperty(BITES, 3) || state == this.blockState.getBaseState().withProperty(BITES, 4)){
			return Items.slime_ball;
		}else{
			return Items.sugar;
		}
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(BITES, meta);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer()
	{
		return EnumWorldBlockLayer.CUTOUT;
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(BITES);
	}

	@Override
	protected BlockState createBlockState()
	{
		return new BlockState(this, BITES);
	}

	@Override
	public int getComparatorInputOverride(World worldIn, BlockPos pos)
	{
		return (7 - worldIn.getBlockState(pos).getValue(BITES)) * 2;
	}

	@Override
	public boolean hasComparatorInputOverride()
	{
		return true;
	}
}
