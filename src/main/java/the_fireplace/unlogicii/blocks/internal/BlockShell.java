package the_fireplace.unlogicii.blocks.internal;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
/**
 * 
 * @author The_Fireplace
 * 
 */
public class BlockShell extends Block {

	public BlockShell() {
		super(Material.anvil);
		setUnlocalizedName("shell");
		setBlockUnbreakable();
		setResistance(131072);
	}

	@Override
	public int quantityDropped(Random random)
	{
		return 0;
	}
}
