package the_fireplace.unlogicii.blocks.internal;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.tile.TileEntitySmartCoalGun;
/**
 * 
 * @author The_Fireplace
 * 
 */
public class DummyBlockSmartCoalGun extends Block implements ITileEntityProvider {
	public DummyBlockSmartCoalGun(){
		super(Material.clay);
		setUnlocalizedName("smart_coal_gun");
		setCreativeTab(UnLogicII.TabUnLogicII);
		disableStats();
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side)
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer()
	{
		return EnumWorldBlockLayer.CUTOUT;
	}

	@Override
	public boolean hasTileEntity(IBlockState state)
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(World world, IBlockState state)
	{
		return new TileEntitySmartCoalGun();
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntitySmartCoalGun();
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess a, BlockPos pos, EnumFacing side){
		return false;
	}
	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		return false;
	}
}
