package the_fireplace.unlogicii.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import the_fireplace.unlogicii.UnLogicII;

public class ULBlock extends Block {

	public ULBlock(Material materialIn) {
		super(materialIn);
		setCreativeTab(UnLogicII.TabUnLogicII);
	}
}
